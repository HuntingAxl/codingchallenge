# Introduction
A tool given a CSV containing pairs of absolute paths of images, as given below. Supported Formats are written [here](https://docs.opencv.org/4.2.0/d4/da8/group__imgcodecs.html#ga288b8b3da0892bd651fce07b3bbd3a56).

| image1 | image2 |
| ------ | ------ |
| aa.png | ba.png |
| ab.png | bb.png |
| ac.png | ac.gif |

Output another CSV file containing the image path pairs and in addition to that, provide the [Similarity](Docs/Similarity.md) score and the time taken to compute the similarity score.

| image1 | image2 | similarity | elapsed |
| ------ | ------ | ---------- | ------- |
| aa.png | ba.png | 0          | 0.006   |
| ab.png | bb.png | 0.23       | 0.843   |
| ac.png | ac.gif | 0          | 1.43    |

# Tech Stack
- Python 3
- OpenCV

# Prerequisites Commands
For Windows:

<code>python -m pip install opencv-python --user</code>

For Linux or MacOS:

<code>pip install opencv-python --user</code>

**NOTE:** Replace 'python' and 'pip' with 'python3' and 'pip3' in the above commands if you have both python 2.x and python 3.x installed in your system.

# Usage Instructions
Download the code, extract it and open a terminal in that folder and execute the below command.

<code>python main.py <csv_filename></code>

For More arguments, you can run:

<code>python main.py -h</code>