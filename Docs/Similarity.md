# Similarity
**Hint:** Visually similar, but not necessarily the binary data.

## Definition
> Ratio of the number of matches between the two images and the maximum number of keypoints/descriptors of the two images.

# Motivation
