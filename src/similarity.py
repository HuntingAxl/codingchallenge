import cv2
import numpy as np

def formatSimilarity(value):
  '''
    Format the Similarity Metric.
  '''
  similarityFormat = '{:.3f}'
  return similarityFormat.format(value)

def preProcessImage(image, blurAmount=5):
  '''
    Reduce Noise present in the image by blurring, which can cause problem in the similarity algorithm.
  '''
  assert blurAmount > 0 and blurAmount%2 > 0
  kernelDimensions = (blurAmount, blurAmount)
  
  blurKernel = np.ones(kernelDimensions, np.uint8)/(blurAmount**2)
  ddepth = -1
  return cv2.filter2D(image, ddepth, kernel=kernelDimensions)

# Function to handle the computation of Similarity metric
def similarityMetric(matches, des1, des2):
  '''
    Function to compute the similarity.
  '''
  max_visual_keypoints = max([len(des1),len(des2)])
  return len(matches)/max_visual_keypoints if (max_visual_keypoints > 0) else 0

# Computes the Similarity of the image 
def computeSimilarity(image1, image2):
  '''
    Computes the similarity between two images using ORB feature matching.
  '''
  # Similarity logic goes here.
  orb = cv2.ORB_create()
  
  # Extract the keypoints and descriptors from the image.
  # Keypoints were not needed in the use case.
  _, des1 = orb.detectAndCompute(image1, None)
  _, des2 = orb.detectAndCompute(image2, None)

  # Use a Matcher to match the features/descriptors.
  # We will be using Hamming Distance since ORB uses that while computing the descriptors.
  # Cross check has been set to true to compute the intersecting set of features.
  bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
  matches = bf.match(des1, des2)

  return similarityMetric(matches, des1, des2)
