from csv import reader, writer
from os import path, getcwd 
from sys import exit 
from cv2 import imread, IMREAD_GRAYSCALE

def getCSVData(filename):
  '''
    Read the CSV Data and return it in list.
  '''
  try:
    with open(filename) as csvfile:
      fileReader = reader(csvfile, delimiter=',')
      return [row for row in fileReader]
  except:
    raise

def writeCSVData(filename, data):
  '''
    Gets the data and filename and saves it in csv format.
  '''
  try:
    with open(filename, mode='w', newline='') as csvfile:
      writer(csvfile).writerows(data)
  except:
    raise

def checkValidPath(filepath):
  '''
    Helps differentiate between the paths according to OS.
    Additionally, makes sure that it is not a folder with name like a file.
  '''
  return path.exists(filepath) and path.isfile(filepath)

def getImagesData(imageList):
  '''
    Read the names of the image files. Load them.
    And then return them back in the form of a list.
    If the image could not be loaded or has zero data,
    then 'None' is returned in its place.
  '''
  images = []
  for imagePath in imageList:
    if(checkValidPath(imagePath)):
      imageData = imread(imagePath, IMREAD_GRAYSCALE)
      imageData = None if imageData.size == 0 else imageData
      images.append(imageData)
    else:
      images.append(None)
  return images