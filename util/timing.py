from datetime import datetime
# accepts a time delta object and formats it according to the specified format in the string.
# Current Format: '<seconds>.<milliseconds>'
# Milliseconds will always occupy 3 spaces.
def formatTimeElapsed(time):
  secondInMilliseconds = 1000
  timeFormat = '{seconds:d}.{microseconds:0>3d}'
  return timeFormat.format(seconds=time.seconds
      ,microseconds=time.microseconds//secondInMilliseconds
    )

# For the current use case, currently working with simple arguments and no named arguments.
# It won't take more than 86,399 seconds to execute.
def execAndTime(func, args):
  # Fix the timing computation.
  before_time = datetime.now()
  result = func(*args)
  return formatTimeElapsed(datetime.now() - before_time), result
