from argparse import ArgumentParser

def handleArguments():
  '''
    Returns the arguments extracted from the arguments parser.
  '''
  parser = getArgumentParser()
  args = parser.parse_args()
  return args

def getArgumentParser():
  ''' 
    Returns the ArgumentParser to be used for Arguments Parsing.
  '''
  # Default filename.
  defaultFilename = 'output.csv'
  
  parser = ArgumentParser(description='Compute Similarity between pairs of images.'
  )
  parser.add_argument('in_csv',
    help="Name of the CSV file containing the image paths."
  )
  parser.add_argument("-o", "--out", action="store", nargs=1, default=defaultFilename,
    help="Changes the name of the output from the default value of '"+defaultFilename+"'"
  )
  return parser