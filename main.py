from util.argParser import handleArguments
from util.helper import getCSVData, writeCSVData, getImagesData
from util.timing import execAndTime
from src.similarity import computeSimilarity

# Main function which handles the purpose of the tool.
def computation(filename, outputFilename):
  data = getCSVData(filename)
  
  # Add the additional headers for the output file.
  data[0].append('similarity')
  data[0].append('elapsed')
  
  # Skip the Data headers.  
  for row in data[1:]:
    # Read the Image.
    image1, image2 = getImagesData(row)

    # Check if both the images have been read and have some data to compute.
    if image1 is None or image2 is None:    
      emptySymbol = '-'

      # Fill in the empty symbol since similarity can't be measured due to unavailability of data.
      row.append(emptySymbol)
      row.append(emptySymbol)
    else:
      # Both images are present, so it is safe to compute the values now.
      timeElapsed, similarity = execAndTime(computeSimilarity, [image1, image2])
      row.append(similarity)
      row.append(timeElapsed)
      
      # Cleanup
      del image1
      del image2
  writeCSVData(outputFilename, data)

if __name__ == '__main__':
  # Get all the arguments from the shell.
  args = handleArguments()

  computation(args.in_csv, args.out)